const IS_DEV = process.env.NODE_ENV !== 'production';
let host = window.location.host; //获取地址和端口
//Request url
export const API_ROOT = IS_DEV ? 'http://127.0.0.1:8001/api/' : 'http://'+host+'/api/';
//Request response time
export const API_TIME = IS_DEV ? '6000' : '8000';
//运行环境（true:正式环境，false:测试环境）
export const RUN_DEV = false;
