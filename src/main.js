import Vue from 'vue'
import App from './app.vue'
import router from './router'
import Vuex from 'vuex'
import store from './vuex/store.js'
import i18n from './i18n/i18n'
import "babel-polyfill";
import axios from 'axios'
import {fetch, post, put} from './api/https'

import { L } from 'vue2-leaflet';
//import 'leaflet.icon.glyph';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

//定义全局变量
Vue.prototype.$fetch = fetch;
Vue.prototype.$post = post;
Vue.prototype.$put = put;

Vue.use(Vuex);

/**引入css文件**/
import './assets/css/base.css'
import 'leaflet/dist/leaflet.css'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/ele-base.less'

Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/**
 *  创建实例
 */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  components: {App},
  template: '<App/>'
});
